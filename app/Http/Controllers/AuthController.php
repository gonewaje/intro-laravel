<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
		return view('form');
	}
	public function welcome(Request $request){
		//return view('welcome');
	}
	public function welcome_post(Request $request){
		//dd($request->all());
		$namadepan = $request->frontname;
		$namabelakang = $request->backname;
		return view('welcome', compact('namadepan', 'namabelakang'));
	}
}
