<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Tanya;
use Auth;
use PDF;


class PertanyaanController extends Controller
{
    //Memberikan middleware agar hanya user yang telah login yang dapat akses link tersebut
    //Memberikan pengecualian diroute tertentu tambahkan ->except(['route pengecualian']);
    //Untuk memberikan hanya satu/beberapa route saja gunakan ->only
    public function __construct(){
        $this->middleware('auth')->only(['create', 'edit', 'update', 'store']);
    }

    public function create(){
        return view('pertanyaan.create');
    }
    public function store(Request $request){
    //dd($request->all());
    $request->validate([
        'judul' => 'required|unique:pertanyaan',
        'isi' => 'required'
    ]);
    //Menggunakan DB Query builder
    //$query = DB::table('pertanyaan')->insert([
    //    "judul" => $request["judul"],
    //    "isi" => $request["isi"]
    //]);

    //Menggunakan ORM
    /*
    $tanya = new Tanya;
    $tanya->judul = $request["judul"];
    $tanya->isi = $request["isi"];
    $tanya->save();
    */

    //Store data dengan mass assignment ORM, harus mendeklarasikan fillable di moddel Tanya.php

    //Menambahkan userid di postingan/pertanyaan dengan auth id
    $tanya = Tanya::create([
        "judul" => $request["judul"],
        "isi" => $request["isi"],
        "user_id" => Auth::id()
    ]);

    return redirect('pertanyaan')->with('success', 'Data Berhasil Disimpan!');
    }
    public function index(){
        //$pertanyaan = DB::table('pertanyaan')->get();

        //Menggunakan ORM model
        //Menanmbah 1 user punya banyak post atau hanya menampilkan post user itu saja
        $user = Auth::user();
        $pertanyaan = $user->pertanyaans;
        //$pertanyaan = Tanya::all();
        return view('pertanyaan.index', compact('pertanyaan'));
    }
    public function show($id){
        //$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        //Menggunakan retrieving model single/mengambil data
        $pertanyaan = Tanya::find($id);
        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function edit($id){
        //$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        //Menggunakan retrieving model single/mengambil data
        $pertanyaan = Tanya::find($id);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }
    public function update($id, Request $request){
        $request->validate([
        'judul' => 'required|unique:pertanyaan',
        'isi' => 'required'
        ]);
        //$query = DB::table('pertanyaan')
        //        ->where('id', $id)
        //        ->update([
        //                'judul' => $request['judul'],
        //                'isi' => $request['isi']
        //            ]);

        //Menggunakan ORM
        $update = Tanya::where('id', $id)->update([
            "judul" => $request['judul'],
            "isi" => $request['isi']
        ]);
        return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan!');
    }
    public function destroy($id){
        //$query = DB::table('pertanyaan')->where('id', $id)->delete();

        //Menggunakan ORM
        Tanya::destroy($id);
        return redirect('/pertanyaan')->with('success', 'pertanyaan berhasil dihapus!');
    }
    public function pdf(){
    $pertanyaan = "Seharusnya ambil Data";
    $pdf = PDF::loadView('pertanyaan.pdf', compact('pertanyaan'));
    return $pdf->download('show.pdf');
    }
}