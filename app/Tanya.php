<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tanya extends Model
{
    protected $table ="pertanyaan";

    //Mass assignment ORM deklarasi fillable (membaca white list)
    //protected $fillable = ["judul", "isi"];

    //Menggunakan Guardian (massal dengan mengabaikan black list)
    protected $guarded = [];
    //Diatas menandakan semua tabel boleh diisi, jika diberikan list "judul" maka judul tidak boleh diisi
    //Hanya perlu menambahkan black list tanya merubah controller
    public function author(){
    	return $this->belongsTo('App\User', 'user_id');
    }
}
