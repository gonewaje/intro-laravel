<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('home');
}); */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::get('/welcome', 'AuthController@welcome');

Route::post('/welcome', 'AuthController@welcome_post');

Route::get('/master', function(){
	return view('/layoutadminlte/master');
});

/*Route::get('/', function(){
	return view('/homeroot/root');
});
*/
Route::get('/data-tables', function(){
	return view('/data_tables');
});

/*
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index')->name('index');
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');
*/

//Menggunakan controller resource, lebih praktis dalam membuat route
//Route::resource('posting', 'PostingController');

//Menggunakan route named, berdasarkan route dengan nama yang sama
Route::resource('pertanyaan', 'PertanyaanController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test-dompdf', function(){

	$pdf = App::make('dompdf.wrapper');
	$pdf->loadHTML('<h1>Test</h1>');
	return $pdf->stream();
});

Route::get('/pdfview', 'PertanyaanController@pdf');