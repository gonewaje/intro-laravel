<!DOCTYPE html>
<html>
<head>
	<title>Sign Up</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="POST">
		@csrf
		<label for="fname">First name:</label><br><br>
		<input type="text" placeholder="First Name" id="fname" name="frontname"><br><br>
		<label for="lname">Last name:</label><br><br>
		<input type="text" placeholder="Last Name" id="lname" name="backname"><br><br>
		<label>Gender:</label><br><br>
		<input type="radio" name="gender" value="0" checked> Male<br>
		<input type="radio" name="gender" value="1"> Female<br>
		<input type="radio" name="gender" value="2"> Other<br><br>
		<label>Nationality</label><br><br>
		<select name="nationality">
			<option value="indonesian">Indonesian</option>
		 	<option value="afghan">Afghan</option>
		  	<option value="albanian">Albanian</option>
		  	<option value="algerian">Algerian</option>
		  	<option value="american">American</option>
		  	<option value="andorran">Andorran</option>
		  	<option value="angolan">Angolan</option>
		  	<option value="antiguans">Antiguans</option>
			<option value="argentinean">Argentinean</option>
			<option value="armenian">Armenian</option>
			<option value="australian">Australian</option>
		</select><br><br>
		<label>Language Spoken:</label><br><br>
		<input type="checkbox" name="language[]" value="0" checked=""> Bahasa Indonesia<br>
		<input type="checkbox" name="language[]" value="0"> English<br>
		<input type="checkbox" name="language[]" value="0"> Other<br><br>
		<label>Bio:</label><br><br>
		<textarea cols="100" rows="7" id="bio" name="bio'"></textarea><br><br>
		<input type="submit" value="Sign Up">
	</form>
</body>
</html>