@extends('layoutadminlte.master')

@section('content')
	<div class='ml-3 mt-2'>
		<div class="car card-primary">
	        <div class="card-header">
	          <h3 class="card-title">Give Me a Question!</h3>
	        </div>
	        <!-- /.box-header -->
	        <!-- form start -->
	        <form role="form" action="/pertanyaan" method="post">
	        	@csrf
	          <div class="card-body">
	            <div class="form-group">
	              <label for="tile">Judul</label>
	              <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Judul" required>
	              @error('judul')
	              	<div class="alert alert-danger">{{ $message }}</div>
	              @enderror
	            </div>
	            <div class="form-group">
	              <label for="body">Isi</label>
	              <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi', '')}}" placeholder="Isi" required>
	              @error('isi')
	              	<div class="alert alert-danger">{{ $message }}</div>
	              @enderror
	            </div>
	          </div>
	          <!-- /.box-body -->

	          <div class="box-footer">
	            <button type="submit" class="btn btn-primary">Create</button>
	          </div>
	        </form>
	    </div>
	</div>
@endsection