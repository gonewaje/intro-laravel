<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('pertanyaan');
        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id')
                    ->unique()
                    ->unsigned();
            $table->timestamps();
            $table->string('judul')
                    ->nullable();
            $table->longText('isi')
                    ->nullable();
            $table->date('tanggal_dibuat')
                    ->nullable();
            $table->date('tanggal_diperbaharui')
                    ->nullable();
            $table->integer('jawaban_tepat_id')
                    ->nullable();
            $table->integer('profile_id')
                    ->nullable();
            $table->bigInteger('user_id')
                    ->unsigned();
            //$table->primary('id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaan');
    }
}
